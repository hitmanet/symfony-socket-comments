const axios = require('axios')


module.exports = async (config, socket) => {
    axios.defaults.headers.common['Autorization'] = config.token
    axios.defaults.headers.common['Content-type'] = 'application/json'
    const service = axios.create({})
    try {
        const response = await service(config)
        return response
    } catch(error) {
        console.log(error.response)
        socket.emit('errorEvent', error.response)
        return false
    }
}