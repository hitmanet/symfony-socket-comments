const axios = require('axios')

module.exports = async (token) => {
    try {
        const { data } = await axios.get(`${process.env.API_HOST}/api/current`, {
            headers: {
                'Authorization': token
            }
        })
        return data
    } catch(error) {
        return false
    }
}