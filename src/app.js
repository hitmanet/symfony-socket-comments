require('dotenv').config()
const app = require('express')()
const router = require('express').Router()
const http = require('http').Server(app)
const io = require('socket.io')(http, { serveClient: false })
const auth = require('./helpers/auth')
const bodyParser = require('body-parser')
const createComment = require('./actions/CreateComment')
const deleteComment = require('./actions/DeleteComment')

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))


const clients = []

app.route('/check').get((_, res) =>{
    return res.send({ 'ms': 'ok' })
})

io.sockets.on('connection', async (socket) => {
    const { auth : token } = socket.handshake.query
    if (!token) return socket.disconnect()
    const user = await auth(token)
    if (!user) return socket.disconnect()
    const currentClient = {
        socketId: socket.id,
        userId: user.id,
    }
    if(global.revision){
        socket.emit('revision', global.revision);
    }
    clients.push(currentClient)
    socket.on('disconnect', () => {
        clientKey = clients.findIndex(user => user.socketId == socket.id)
        clients.splice(clientKey)
    })
    socket.on('createComment', async (data) => await createComment(io, socket, data, token))
    socket.on('deleteComment', async (data) => await deleteComment(io, socket, data, token))
})

http.listen(process.env.PORT || process.env.APP_PORT, () => {
    console.log(`listening on ${process.env.APP_PORT} port`)
})
