const axios = require('axios')

module.exports = async (io, socket, data, token) => {
    try {
        const response = await axios.post(`${process.env.API_HOST}/api/comments`, data, {
            headers: {
                'Authorization': token
            }
        })
        return socket.broadcast.emit('newComment', { ...response.data })
    } catch(error) {
        return socket.emit('errorInCommentCreated', { ...error.response.data })
    }
}