const axios = require('axios')

module.exports = async (io, socket, data, token) => {
    try {
        const response = await axios.delete(`${process.env.API_HOST}/api/comments/${data.id}`, {
            headers: {
                'Authorization': token
            }
        })
        return socket.broadcast.emit('commentDeleted', { ...response.data })
    } catch(error) {
        return socket.emit('errorInCommentDeleted', { ...error.response.data })
    }
}